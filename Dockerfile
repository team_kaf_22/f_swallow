FROM python:3.5

RUN apt-get update && apt-get upgrade -y

RUN mkdir /data
WORKDIR /data
COPY requirements.txt /data
RUN pip install -r ./requirements.txt
COPY . /data
RUN python3 manage.py migrate
CMD ["python","manage.py","runserver","0.0.0.0:8000"]