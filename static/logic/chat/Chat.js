

//Клік на менюху абонентів
$(function () {
    $("#rectangleButton").click(function () {
        if ($("#contacts").css("display") == "none") {
            $("#contacts").css("display", "block");
            $("#vector").css("transform", "rotate(0deg)");
            if ($(window).width() < 576)
                $("#chatContainer").css("display", "none");
            $("#chatContainer").addClass("col-sm-8 col-md-9");
        }
        else {
            $("#contacts").css("display", "none");
            $("#vector").css("transform", "rotate(180deg)");
            $("#chatContainer").css("display", "block");
            $("#chatContainer").removeClass("col-sm-8 col-md-9");
        }
    })
});

//Логіка адаптації морди до розмірів вікна
$(function () {
    $(window).resize(function () {
        let Height = $(window).height() - 60;
        $("#contacts").css("height", Height.valueOf());
        Height -= 120;
        $('#chat').css("height", Height.valueOf());
        if ($(window).width() < 576) {
            $("#chatContainer").css("display", "none");
            $("#sendButton").css("height", "40px");
            $("#sendButton").css("margin-top", "10px");
            $("#addContentIcon").css("margin-left", "-10px");
        }
        else {
            $("#chatContainer").css("display", "block");
            $("#sendButton").css("height", "50px");
            $("#sendButton").css("margin-top", "5px");
            $("#addContentIcon").css("margin-left", "10px");
        }
    })
})


//Задання параметрів під час запуску
//Задання розмірів окремих елементів
refreshColors();
let Height = $(window).height() - 60;
$("#contacts").css("height", Height.valueOf());
Height -= 120;
$('#chat').css("height", Height.valueOf());
//Приховання #chatContainer за замовчуванням на смартфонах
if ($(window).width() < 576) {
    $("#chatContainer").css("display", "none");
    $("#sendButton").css("height", "40px");
    $("#sendButton").css("margin-top", "10px");
    $("#addContentIcon").css("margin-left", "-10px");
}

//ТЕСТ
showAbonent("suhoputni.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sso.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sbu.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("mvs.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("vms.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("dps.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("suhoputni.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sso.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sbu.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("mvs.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("vms.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("dps.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("suhoputni.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sso.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sbu.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("mvs.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("vms.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("dps.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("suhoputni.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sso.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("sbu.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("mvs.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("vms.png", "Lorem ipsum","jksdfjnsfnsjdfns");
showAbonent("dps.png", "Lorem ipsum","jksdfjnsfnsjdfns");
