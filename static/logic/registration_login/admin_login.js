/*
 *Real time checking user input
 */
function CustomValidation(input) {
    this.invalidities = [];
    this.validityChecks = [];
    this.inputNode = input;
    this.registerListener();
}

CustomValidation.prototype = {
    addInvalidity: function (message) {
        this.invalidities.push(message);
    },
    getInvalidities: function () {
        return this.invalidities.join('. \n');
    },
    checkValidity: function (input) {
        for (let i = 0; i < this.validityChecks.length; i++) {
            let isInvalid = this.validityChecks[i].isInvalid(input);
            if (isInvalid) {
                this.addInvalidity(this.validityChecks[i].invalidityMessage);
            }
            let requirementElement = this.validityChecks[i].element;
            if (requirementElement) {
                if (isInvalid) {
                    requirementElement.classList.add('invalid');
                    requirementElement.classList.remove('valid');
                } else {
                    requirementElement.classList.remove('invalid');
                    requirementElement.classList.add('valid');
                }
            }
        }
    },
    checkInput: function () {
        this.inputNode.CustomValidation.invalidities = [];
        this.checkValidity(this.inputNode);
        if (this.inputNode.CustomValidation.invalidities.length === 0 && this.inputNode.value !== '') {
            this.inputNode.setCustomValidity('');
        } else {
            let message = this.inputNode.CustomValidation.getInvalidities();
            this.inputNode.setCustomValidity(message);
        }
    },
    registerListener: function () {
        let CustomValidation = this;
        this.inputNode.addEventListener('keyup', function () {
            CustomValidation.checkInput();
        });
    }
};

let tokenSuperAdminValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(/[0-9]/g);
        },
        invalidityMessage: 'At least 1 number is required',
        element: document.querySelector('label[for="access_token"] .input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[a-z]/g);
        },
        invalidityMessage: 'At least 1 lowercase letter is required',
        element: document.querySelector('label[for="access_token"] .input-requirements li:nth-child(2)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[A-Z]/g);
        },
        invalidityMessage: 'At least 1 uppercase letter is required',
        element: document.querySelector('label[for="access_token"] .input-requirements li:nth-child(3)')
    }
];

/*
 *Super admin login input
 */
let tokenSuperAdminInput = document.getElementById('access_token');

tokenSuperAdminInput.CustomValidation = new CustomValidation(tokenSuperAdminInput);
tokenSuperAdminInput.CustomValidation.validityChecks = tokenSuperAdminValidityChecks;

let inputs = document.querySelectorAll('input:not([type="submit"])');
let submit = document.querySelector('input[type="submit"]');
let form = document.getElementById('login_form');

function validate() {
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].CustomValidation.checkInput();
    }
}

submit.addEventListener('click', validate);
form.addEventListener('submit', validate);