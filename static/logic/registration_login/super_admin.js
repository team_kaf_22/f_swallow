let token_counter = $("#token_counter");
token_counter.val(0);

let power_structure = $("#power_structure");
power_structure.val("---Power structure---");

let text_area_token = $("#token");

function generate_tokens() {
    text_area_token.val("");
    let power_structure_val = power_structure.val();
    let count_of_token = token_counter.val();
    let generate = $("#generate");
    generate.prop("disabled", false);
    if (power_structure_val === '---Power structure---') {
        $.dreamAlert({
            'type': 'error',
            'message': 'Choose power structure!',
            'position': 'right',
        });
        generate.prop("disabled", false);
    } else {
        if (count_of_token <= 0) {
            $.dreamAlert({
                'type': 'error',
                'message': 'Negative count of tokens!',
                'position': 'right',
            });
        } else {
            let tokens = [];
            for (let i = 0; i < count_of_token; i++) {
                tokens.push(generator(power_structure_val));
            }
            for (let i = 0; i < tokens.length; i++) {
                text_area_token.val(text_area_token.val() + "\n" + tokens[i]);
            }
            token_counter.val(0);
        }
    }
}

function check_tokens(tokens) {
    for (let i = 0; i < tokens.length; i++) {
        for (let j = i + 1; j < tokens.length; j++) {
            if (tokens[i] === tokens[j]) {
                tokens.slice(tokens[j], 1);
            }
        }
    }
    return tokens;
}

function generator(power_structure) {
    return power_structure + '_' + makeToken();
}

function makeToken() {
    let token = "";
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 10; i++) {
        token += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return token;
}

/*
 *Real time checking user input
 */
function CustomValidation(input) {
    this.invalidities = [];
    this.validityChecks = [];
    this.inputNode = input;
    this.registerListener();
}

CustomValidation.prototype = {
    addInvalidity: function (message) {
        this.invalidities.push(message);
    },
    getInvalidities: function () {
        return this.invalidities.join('. \n');
    },
    checkValidity: function (input) {
        for (let i = 0; i < this.validityChecks.length; i++) {
            let isInvalid = this.validityChecks[i].isInvalid(input);
            if (isInvalid) {
                this.addInvalidity(this.validityChecks[i].invalidityMessage);
            }
            let requirementElement = this.validityChecks[i].element;
            if (requirementElement) {
                if (isInvalid) {
                    requirementElement.classList.add('invalid');
                    requirementElement.classList.remove('valid');
                } else {
                    requirementElement.classList.remove('invalid');
                    requirementElement.classList.add('valid');
                }
            }
        }
    },
    checkInput: function () {
        this.inputNode.CustomValidation.invalidities = [];
        this.checkValidity(this.inputNode);
        if (this.inputNode.CustomValidation.invalidities.length === 0 && this.inputNode.value !== '') {
            this.inputNode.setCustomValidity('');
        } else {
            let message = this.inputNode.CustomValidation.getInvalidities();
            this.inputNode.setCustomValidity(message);
        }
    },
    registerListener: function () {
        let CustomValidation = this;
        this.inputNode.addEventListener('keyup', function () {
            CustomValidation.checkInput();
        });
    }
};

let newTokenSuperAdminValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(/[0-9]/g);
        },
        invalidityMessage: 'At least 1 number is required',
        element: document.querySelector('label[for="new_token"] .input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[a-z]/g);
        },
        invalidityMessage: 'At least 1 lowercase letter is required',
        element: document.querySelector('label[for="new_token"] .input-requirements li:nth-child(2)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[A-Z]/g);
        },
        invalidityMessage: 'At least 1 uppercase letter is required',
        element: document.querySelector('label[for="new_token"] .input-requirements li:nth-child(3)')
    }
];

let newTokenSuperAdminRepeatValidityChecks = [
    {
        isInvalid: function () {
            return newTokenSuperAdminInput.value !== repeatTokenSuperAdminInput.value;
        },
        invalidityMessage: 'This input needs to be like another token',
        element: document.querySelector('label[for="change_token_repeat"] .input-requirements li:nth-child(1)')
    }
];

/*
 *Super admin login input
 */
let newTokenSuperAdminInput = document.getElementById('new_token');
let repeatTokenSuperAdminInput = document.getElementById('change_token_repeat');

newTokenSuperAdminInput.CustomValidation = new CustomValidation(newTokenSuperAdminInput);
newTokenSuperAdminInput.CustomValidation.validityChecks = newTokenSuperAdminValidityChecks;

repeatTokenSuperAdminInput.CustomValidation = new CustomValidation(repeatTokenSuperAdminInput);
repeatTokenSuperAdminInput.CustomValidation.validityChecks = newTokenSuperAdminRepeatValidityChecks;

let inputs = document.querySelectorAll('input:not([type="submit"])');
let submit = document.querySelector('input[type="submit"]');
let form = document.getElementById('change_super_admin_token');

function validate() {
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].CustomValidation.checkInput();
    }
}

submit.addEventListener('click', validate);
form.addEventListener('submit', validate);

$(document).ready(function () {
    load_tables();
    $("#structures tbody tr").dblclick(function () {
       alert("DB");
    });
});

function addRow(tableID) {
    let table = document.getElementById(tableID);
    let rowCount = table.rows.length;
    let row = table.insertRow(rowCount);
    let colCount = table.rows[0].cells.length;
    let text = prompt("Enter power structure name", "");
    for (let i = 0; i < colCount; i++) {
        if (text === null)
            break;
        else {
            let new_cell = row.insertCell(i);
            new_cell.innerHTML = table.rows[0].cells[i].innerHTML;
            switch (new_cell.childNodes[0].type) {
                case "text":
                    new_cell.childNodes[0].value = text;
                    $(".table_input").addClass("table_data table_input");
                    break;
                case "checkbox":
                    new_cell.childNodes[0].checked = false;
                    break;
            }
        }
    }
    add_new_structures(text);
}

function deleteRow(tableID) {
    try {
        let table = document.getElementById(tableID);
        let rowCount = table.rows.length;
        let data = [];
        for (let i = 0; i < rowCount; i++) {
            let row = table.rows[i];
            let check_box = row.cells[0].childNodes[0];
            if (null != check_box && check_box.checked) {
                data.push(row.cells[1].childNodes[0].value);
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        }
        if (data.length > 0) {
            delete_power_structure(data);
        } else {
            $.dreamAlert({
                'type': 'error',
                'message': 'Select structures for deleting!',
                'position': 'right',
            });
        }
    } catch (e) {
        alert(e);
    }
}

function saveTable(tableID) {
    $.dreamAlert({
        'type': 'success',
        'message': 'All changed data saved to DB!',
        'position': 'right',
    });
}