//RegExp for checking IP address
let regexServerIP = new RegExp(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g);

let powerStructureArray = {
    'default': ['UGF', 'UAF', 'SSO', 'DshV', 'NGU', 'SBGS', 'SSU'],
    'un': ['UN'],
    'police': ['Police'],
    'pou': ['POU'],
    'sesu': ['SESU'],
    'none': ['NSDC', 'MIA', 'SMS', 'MJU', 'NABU'],
};

let runkArray = {
    'default': {
        'OR-1': ['Soldier/Private'],
        'OR-2': ['Senior solder'],
        'OR-3': ['/Corporal'],
        'OR-4': ['Junior sergeant/Sergeant'],
        'OR-5': ['/Senior sergeant'],
        'OR-6': ['Sergeant/Master sergeant'],
        'OR-7': ['Senior sergeant/Staff sergeant'],
        'OR-8': ['Foreman/Master sergeant'],
        'OR-9': ['Ensign/Senior master sergeant', 'Senior ensign/Master master sergeant'],
        'OF-1': ['Junior lieutenant', 'Lieutenant', 'Senior lieutenant'],
        'OF-2': ['Captain'],
        'OF-3': ['Major'],
        'OF-4': ['Lieutenant colonel'],
        'OF-5': ['Colonel'],
        'OF-6': ['/Brigadier general'],
        'OF-7': ['Major general'],
        'OF-8': ['Lieutenant general'],
        'OF-9': ['Colonel general'],
        'OF-10': ['General of the Army of Ukraine'],
    },
    'un': {
        'OR-1': ['Sailor'],
        'OR-2': ['Senior sailor'],
        'OR-3': ['/Chief of the II rank'],
        'OR-4': ['Sergeant of the II rank/Foreman'],
        'OR-5': ['/Chief foreman'],
        'OR-6': ['Foreman and articles/Chief ship sergeant'],
        'OR-7': ['Chief foreman/Staff chief'],
        'OR-8': ['Chief ship sergeant/Master foreman'],
        'OR-9': ['Midshipman/Senior master sergeant', 'Senior midshipman/Chief master sergeant'],
        'OF-1': ['Junior lieutenant', 'Lieutenant', 'Senior lieutenant'],
        'OF-2': ['Captain-lieutenant'],
        'OF-3': ['Captain of the III rank'],
        'OF-4': ['Captain of the II rank'],
        'OF-5': ['Captain of the I rank'],
        'OF-6': ['/Brigadier general'],
        'OF-7': ['Rear admiral'],
        'OF-8': ['Vice admiral'],
        'OF-9': ['Admiral'],
    },
    'police': {
        'OR-1': ['Private police'],
        'OR-3': ['Corporal police'],
        'OR-4': ['Police sergeant'],
        'OR-5': ['Senior police sergeant'],
        'OF-1': ['Junior lieutenant of the police', 'Lieutenant of the police', 'Senior lieutenant of the police'],
        'OF-2': ['Police captain'],
        'OF-3': ['Police major'],
        'OF-4': ['Lieutenant colonel of the police'],
        'OF-5': ['Police colonel'],
        'OF-7': ['Police general of 3rd rank'],
        'OF-8': ['Police general of 2rd rank'],
        'OF-9': ['Police general of 1rd rank'],
    },
    'pou': {
        'OF-1': ['Lawyer of the 3rd class', 'Lawyer of the 2rd class'],
        'OF-2': ['Lawyer of the 1rd class'],
        'OF-3': ['Junior counselor of justice'],
        'OF-4': ['Counselor of justice'],
        'OF-5': ['Senior counselor of justice'],
        'OF-7': ['State counselor of justice of the 3rd rank'],
        'OF-8': ['State counselor of justice of the 2rd rank'],
        'OF-9': ['State counselor of justice of the 1rd rank'],
        'OF-10': ['State counselor of justice of Ukraine'],
    },
    'sesu': {
        'OR-1': ['Private soldier'],
        'OR-3': ['/Corporal'],
        'OR-4': ['Junior sergeant/Sergeant'],
        'OR-5': ['/Senior sergeant'],
        'OR-6': ['Sergeant/Master sergeant'],
        'OR-7': ['Senior sergeant/Staff sergeant'],
        'OR-8': ['Foreman/Master sergeant'],
        'OR-9': ['Ensign/Senior master sergeant', 'Senior ensign/Master master sergeant'],
        'OF-1': ['Junior lieutenant', 'Lieutenant', 'Senior lieutenant'],
        'OF-2': ['Captain'],
        'OF-3': ['Major'],
        'OF-4': ['Lieutenant colonel'],
        'OF-5': ['Colonel'],
        'OF-6': ['/Brigadier general'],
        'OF-7': ['Major general'],
        'OF-8': ['Lieutenant general'],
        'OF-9': ['Colonel general'],
        'OF-10': ['General'],
    },
    'none': [],
};

//Switching from one tab to another
function open_tab(event, tabId) {
    let i, tabContent, tabLinks, registrationTabContent;
    tabContent = document.getElementsByClassName("tab");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("btn1");
    for (i = 0; i < tabLinks.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace(" active", "");
    }
    registrationTabContent = document.getElementsByClassName("tab_sign_up");
    if (tabId === "sign_up_html") {
        registrationTabContent[0].style.display = "block";
    }
    document.getElementById(tabId).style.display = "block";
    event.currentTarget.className += " active";
}

//Switching from one registration page to another
function nextRegistrationTab(event, tabId) {
    let i, tabContent, tabLinks;
    tabContent = document.getElementsByClassName("tab_sign_up");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("next_btn");
    for (i = 0; i < tabLinks.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace(" active", "");
    }
    document.getElementById(tabId).style.display = "block";
    event.currentTarget.className += " active";
}

//Switching from one registration page to previous registration page
function previousRegistrationTab(event, tabId) {
    let i, tabContent, tabLinks;
    tabContent = document.getElementsByClassName("tab_sign_up");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("previous_btn");
    for (i = 0; i < tabLinks.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace(" active", "");
    }
    document.getElementById(tabId).style.display = "block";
    event.currentTarget.className += " active";
}

/*
 *Real time checking user input
 */
function CustomValidation(input) {
    this.invalidities = [];
    this.validityChecks = [];
    this.inputNode = input;
    this.registerListener();
}

CustomValidation.prototype = {
    addInvalidity: function (message) {
        this.invalidities.push(message);
    },
    getInvalidities: function () {
        return this.invalidities.join('. \n');
    },
    checkValidity: function (input) {
        for (let i = 0; i < this.validityChecks.length; i++) {
            let isInvalid = this.validityChecks[i].isInvalid(input);
            if (isInvalid) {
                this.addInvalidity(this.validityChecks[i].invalidityMessage);
            }
            let requirementElement = this.validityChecks[i].element;
            if (requirementElement) {
                if (isInvalid) {
                    requirementElement.classList.add('invalid');
                    requirementElement.classList.remove('valid');
                } else {
                    requirementElement.classList.remove('invalid');
                    requirementElement.classList.add('valid');
                }
            }
        }
    },
    checkInput: function () {
        this.inputNode.CustomValidation.invalidities = [];
        this.checkValidity(this.inputNode);
        if (this.inputNode.CustomValidation.invalidities.length === 0 && this.inputNode.value !== '') {
            this.inputNode.setCustomValidity('');
        } else {
            let message = this.inputNode.CustomValidation.getInvalidities();
            this.inputNode.setCustomValidity(message);
        }
    },
    registerListener: function () {
        let CustomValidation = this;
        this.inputNode.addEventListener('keyup', function () {
            CustomValidation.checkInput();
        });
    }
};

let serverValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(regexServerIP);
        },
        invalidityMessage: 'This input needs to be like IP address',
        element: document.querySelector('label[for="server"] .input-requirements li:nth-child(1)')
    }
];

let usernameValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(/[a-zа-я]_[a-zа-я]/g);
        },
        invalidityMessage: 'Need look like this: name_surname',
        element: document.querySelector('label[for="user_id"] .input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/_/g);
        },
        invalidityMessage: 'Need to consist underscore',
        element: document.querySelector('label[for="user_id"] .input-requirements li:nth-child(2)')
    }
];

let usernameRegValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(/[a-zа-я]_[a-zа-я]/g);
        },
        invalidityMessage: 'Need look like this: name_surname',
        element: document.querySelector('label[for="user_reg_id"] .input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/_/g);
        },
        invalidityMessage: 'Need to consist underscore',
        element: document.querySelector('label[for="user_reg_id"] .input-requirements li:nth-child(2)')
    }
];

let passwordValidityChecks = [
    {
        isInvalid: function (input) {
            return input.value.length < 8 | input.value.length > 25;
        },
        invalidityMessage: 'This input needs to be between 8 and 25 characters',
        element: document.querySelector('label[for="password"] .input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[0-9]/g);
        },
        invalidityMessage: 'At least 1 number is required',
        element: document.querySelector('label[for="password"] .input-requirements li:nth-child(2)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[a-z]/g);
        },
        invalidityMessage: 'At least 1 lowercase letter is required',
        element: document.querySelector('label[for="password"] .input-requirements li:nth-child(3)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[A-Z]/g);
        },
        invalidityMessage: 'At least 1 uppercase letter is required',
        element: document.querySelector('label[for="password"] .input-requirements li:nth-child(4)')
    }
];

let newPasswordValidityChecks = [
    {
        isInvalid: function (input) {
            return input.value.length < 8 | input.value.length > 25;
        },
        invalidityMessage: 'This input needs to be between 8 and 25 characters',
        element: document.querySelector('label[for="new_password"] .input-requirements li:nth-child(1)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[0-9]/g);
        },
        invalidityMessage: 'At least 1 number is required',
        element: document.querySelector('label[for="new_password"] .input-requirements li:nth-child(2)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[a-z]/g);
        },
        invalidityMessage: 'At least 1 lowercase letter is required',
        element: document.querySelector('label[for="new_password"] .input-requirements li:nth-child(3)')
    },
    {
        isInvalid: function (input) {
            return !input.value.match(/[A-Z]/g);
        },
        invalidityMessage: 'At least 1 uppercase letter is required',
        element: document.querySelector('label[for="new_password"] .input-requirements li:nth-child(4)')
    }
];

let newPasswordRepeatValidityChecks = [
    {
        isInvalid: function () {
            return newPasswordInput.value !== repeatedNewPasswordInput.value;
        },
        invalidityMessage: 'This input needs to be this: Name',
        element: document.querySelector('label[for="repeated_new_password"] .input-requirements li:nth-child(1)')
    }
];

let nameValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(/[A-Za-zА-Яа-я]/g);
        },
        invalidityMessage: 'This input needs to be this: Name',
        element: document.querySelector('label[for="first_name"] .input-requirements li:nth-child(1)')
    }
];

let surnameValidityChecks = [
    {
        isInvalid: function (input) {
            return !input.value.match(/[A-Za-zА-Яа-я]/g);
        },
        invalidityMessage: 'This input needs to be this: Surname',
        element: document.querySelector('label[for="last_name"] .input-requirements li:nth-child(1)')
    }
];

let tokenValidityChecks = [
    {
        isInvalid: function (input) {
            return input.value.length < 10;
        },
        invalidityMessage: 'Must include more than 10 symbols',
        element: document.querySelector('label[for="token"] .input-requirements li:nth-child(1)')
    }
];


function onMilitaryRankChange() {
    let select = $("#military_rank");
    select.each(function () {
        return select.val() !== "MR";
    });
}

let militaryRankValidityChecks = [
    {
        isInvalid: function () {
            onMilitaryRankChange();
        },
        invalidityMessage: 'Must be choose one military rank',
        element: document.querySelector('label[for="military_rank"] .input-requirements li:nth-child(1)')
    }
];

let positionValidityChecks = [
    {
        isInvalid: function () {
            return positionInput.value === "";
        },
        invalidityMessage: 'Must be not empty',
        element: document.querySelector('label[for="position"] .input-requirements li:nth-child(1)')
    }
];

/*
 *Sign in input
 */
let serverInput = document.getElementById('server');
let usernameInput = document.getElementById('user_id');
let passwordInput = document.getElementById('password');

serverInput.CustomValidation = new CustomValidation(serverInput);
serverInput.CustomValidation.validityChecks = serverValidityChecks;

usernameInput.CustomValidation = new CustomValidation(usernameInput);
usernameInput.CustomValidation.validityChecks = usernameValidityChecks;

passwordInput.CustomValidation = new CustomValidation(passwordInput);
passwordInput.CustomValidation.validityChecks = passwordValidityChecks;

/*
 *Sign up input
 */
let firstNameInput = document.getElementById("first_name");
let lastNameInput = document.getElementById("last_name");
let userIdInput = document.getElementById("user_reg_id");
let tokenInput = document.getElementById("token");
let militaryRankInput = document.getElementById("military_rank");
let positionInput = document.getElementById("position");
let domainInput = document.getElementById("domain");
let centralDomainInput = document.getElementById("central_domain");
let newPasswordInput = document.getElementById("new_password");
let repeatedNewPasswordInput = document.getElementById("repeated_new_password");

firstNameInput.CustomValidation = new CustomValidation(firstNameInput);
firstNameInput.CustomValidation.validityChecks = nameValidityChecks;

lastNameInput.CustomValidation = new CustomValidation(lastNameInput);
lastNameInput.CustomValidation.validityChecks = surnameValidityChecks;

userIdInput.CustomValidation = new CustomValidation(userIdInput);
userIdInput.CustomValidation.validityChecks = usernameRegValidityChecks;

tokenInput.CustomValidation = new CustomValidation(tokenInput);
tokenInput.CustomValidation.validityChecks = tokenValidityChecks;

militaryRankInput.CustomValidation = new CustomValidation(militaryRankInput);
militaryRankInput.CustomValidation.validityChecks = militaryRankValidityChecks;

positionInput.CustomValidation = new CustomValidation(positionInput);
positionInput.CustomValidation.validityChecks = positionValidityChecks;

domainInput.CustomValidation = new CustomValidation(domainInput);
domainInput.CustomValidation.validityChecks = serverValidityChecks;

centralDomainInput.CustomValidation = new CustomValidation(centralDomainInput);
centralDomainInput.CustomValidation.validityChecks = serverValidityChecks;

newPasswordInput.CustomValidation = new CustomValidation(newPasswordInput);
newPasswordInput.CustomValidation.validityChecks = newPasswordValidityChecks;

repeatedNewPasswordInput.CustomValidation = new CustomValidation(repeatedNewPasswordInput);
repeatedNewPasswordInput.CustomValidation.validityChecks = newPasswordRepeatValidityChecks;

let inputs = document.querySelectorAll('input:not([type="submit"])');
let submit = document.querySelector('input[type="submit"]');
let form = document.getElementById('login_form');

function validate() {
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].CustomValidation.checkInput();
    }
}

submit.addEventListener('click', validate);
form.addEventListener('submit', validate);

function login_as_admin() {
    location.href = 'admin_login/';
}