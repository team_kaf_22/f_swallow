//GET request for signing in
function sign_in() {
    let user = {
        user_id: $("#user_id").val(),
        server: $("#server").val(),
        password: $("#password").val(),
    };
    $.ajax({
        type: "GET",
        cache: false,
        url: "/check_login/",
        contentType: "json",
        async: true,
        data: user,
        success: function (data) {
            alert(data.error);
            if (data.error === null) {
                location.href = data.user + '/chat/';
            } else {
                $.dreamAlert({
                    'type': 'error',
                    'message': 'Something are wrong!',
                    'position': 'right',
                });
            }
        },
        error: function () {
            $.dreamAlert({
                'type': 'error',
                'message': 'Server are not available!',
                'position': 'right',
            });
        }
    });
}

//GET request for signing up
function sign_up() {
    let new_user = {
        firstName: $("#first_name").val(),
        lastName: $("#last_name").val(),
        position: $("#position").val(),
        militaryRank: $("#military_rank").val(),
        domain: $("#domain").val(),
        centralDomain: $("#central_domain").val(),
        password: $("#new_password").val()
    };
    $.ajax({
        type: "GET",
        cache: false,
        url: "/check_registration/",
        contentType: "json",
        async: true,
        data: new_user,
        success: function () {

        },
        error: function () {
            $.dreamAlert({
                'type': 'error',
                'message': 'Server are not available!',
                'position': 'right',
            });
        }
    });
    let tabContent = document.getElementsByClassName("tab_sign_up");
    for (let i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
}

//GET request for signing in for admin
function login_admin() {
    let token = {
        token: $("#access_token").val(),
    };
    $.ajax({
        type: "GET",
        cache: false,
        url: "/admin_login_view/",
        contentType: "json",
        async: true,
        data: token,
        success: function (data) {
            if (data.info === 'super_admin') {
                location.href = 'super_admin_view/';
            }
            else if (data.info === 'admin') {
                let structure = data.structure;
                location.href = structure + '/admin_view/';
            } else {
                $.dreamAlert({
                    'type': 'error',
                    'message': 'Something are wrong!',
                    'position': 'right',
                });
            }
        },
        error: function () {
            $.dreamAlert({
                'type': 'error',
                'message': 'Server are not available!',
                'position': 'right',
            });
        }
    });
}

//GET request for sending super_admin tokens to server
function send_super_admin_tokens() {
    let tokens_area = $("#token");
    let pw_structure = $("#power_structure option:selected");
    let t = tokens_area.val().split('\n');
    let tokens_new = check_tokens(t);
    let length = t.length - tokens_new.length;
    while (length > 0) {
        for (let k = 0; k < length; k++) {
            t.push(generator(pw_structure));
            tokens_new = check_tokens(t);
        }
    }
    let tokens = {
        token: tokens_area.val(),
        structure: pw_structure.text(),
    };
    if (tokens_area.val().length === 0) {
        $.dreamAlert({
            'type': 'error',
            'message': 'Not generated tokens!',
            'position': 'right',
        });
        $("#generate").prop("disabled", true);
    } else {
        $.ajax({
            type: "GET",
            cache: false,
            url: "/super_admin_tokens/",
            contentType: "json",
            async: true,
            data: tokens,
            success: function (data) {
                if (data.count_unique === "0") {
                    $.dreamAlert({
                        'type': 'success',
                        'message': 'Operation completed!',
                        'position': 'right',
                    });
                } else {
                    alert("Some tokens already exists in BD!\nYou need to generate a " + data.count_unique + " tokens!");
                }
            },
            error: function () {
                $.dreamAlert({
                    'type': 'error',
                    'message': 'Server are not available!',
                    'position': 'right',
                });
            }
        });
        tokens_area.val("");
        $("#power_structure").val("---Power structure---");
        $("#generate").prop("disabled", true);
    }
}

//GET request for to server for changing super admin token
function change_token() {
    let new_token = $("#new_token");
    let new_token_repeat = $("#change_token_repeat");
    if (new_token.val() === "" || new_token_repeat.val() === "") {
        $.dreamAlert({
            'type': 'error',
            'message': 'Enter the new token!',
            'position': 'right',
        });
    } else {
        if (new_token.val() !== new_token_repeat.val()) {
            $.dreamAlert({
                'type': 'error',
                'message': 'Entered tokens are not equals!',
                'position': 'right',
            });
        } else {
            let token = {
                token: new_token_repeat.val(),
            };
            $.ajax({
                type: "GET",
                cache: false,
                url: "/super_admin_change_token/",
                contentType: "json",
                async: true,
                data: token,
                success: function (data) {
                    if (data.info === 'success') {
                        $.dreamAlert({
                            'type': 'success',
                            'message': 'Operation completed!',
                            'position': 'right',
                        });
                    } else {
                        alert(data.info);
                    }
                },
                error: function () {
                    $.dreamAlert({
                        'type': 'error',
                        'message': 'Server are not available!',
                        'position': 'right',
                    });
                }
            });
            new_token.val("");
            new_token_repeat.val("");
        }
    }
}

//GET request for loading data to tables from DB
function load_tables() {
    let structure_table = $("#structures");
    let select_list = $("#power_structure");
    $.ajax({
        type: "GET",
        cache: false,
        url: "/load_structure_table/",
        async: true,
        success: function (data) {
            if (data.info === 'success') {
                let power_structures = data.structures;
                for (let i = 0; i < power_structures.length; i++) {
                    let append1 = "<tr><td><input type='checkbox' class='check_data' name='check'/>" +
                        "</td><td class='table_data'><input type='text' class='table_input_super' value='" + power_structures[i] + "'></td></tr>";
                    let append2 = "<option>" + power_structures[i] + "</option>";
                    structure_table.append(append1);
                    select_list.append(append2);
                }
            } else {
                $.dreamAlert({
                    'type': 'error',
                    'message': 'Cant load data to power structure table!',
                    'position': 'right',
                });
            }
        },
        error: function () {
            $.dreamAlert({
                'type': 'error',
                'message': 'Server are not available!',
                'position': 'right',
            });
        }
    });
}

//GET request for delete power structure from DB
function delete_power_structure(data) {
    data += "";
    let structures = {
        value: data,
    };
    $.ajax({
        type: "GET",
        cache: false,
        url: "/delete_structure/",
        contentType: 'json',
        data: structures,
        async: true,
        success: function (data) {
            if (data.info === 'success') {
                $.dreamAlert({
                    'type': 'success',
                    'message': 'Operation completed!',
                    'position': 'right',
                });
            } else {
                $.dreamAlert({
                    'type': 'error',
                    'message': 'Cant delete structures from DB!',
                    'position': 'right',
                });
            }
        },
        error: function () {
            $.dreamAlert({
                'type': 'error',
                'message': 'Server are not available!',
                'position': 'right',
            });
        }
    });
}

//GET request for show admins from chosen power structure
// function show_admins_from_power_structure(pw_structure) {
//     let admins_table = $("#admins");
//     let structure = {
//         structure_name: pw_structure,
//     };
//     $.ajax({
//         type: "GET",
//         cache: false,
//         url: "/show_admins/",
//         contentType: 'json',
//         data: structure,
//         async: true,
//         success: function (data) {
//             if (data.info === 'success') {
//                 let admin = data.admins;
//                 for (let i = 0; i < admin.length; i++) {
//                     admins_table.append("<tr><td class='table_data'>" + admin[i] + "</td></tr>");
//                 }
//             } else {
//                 $.dreamAlert({
//                     'type': 'error',
//                     'message': 'Cant show admins from this power structure!',
//                     'position': 'right',
//                 });
//             }
//         },
//         error: function () {
//             $.dreamAlert({
//                 'type': 'error',
//                 'message': 'Server are not available!',
//                 'position': 'right',
//             });
//         }
//     });
// }

function add_new_structures(text) {
    let data = {
        'new_structure': text,
    };
    $.ajax({
        type: "GET",
        cache: false,
        url: "/add_structure/",
        contentType: 'json',
        data: data,
        async: true,
        success: function (data) {
            if (data.info === 'success') {
                $.dreamAlert({
                    'type': 'success',
                    'message': 'Operation completed!',
                    'position': 'right',
                });
                load_tables();
            } else {
                $.dreamAlert({
                    'type': 'error',
                    'message': 'Cant add new structure to DB!',
                    'position': 'right',
                });
            }
        },
        error: function () {
            $.dreamAlert({
                'type': 'error',
                'message': 'Server are not available!',
                'position': 'right',
            });
        }
    });
}