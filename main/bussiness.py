from matrix_client.api import MatrixHttpApi
from matrix_client.client import MatrixClient
import time
from matrix_client.room import Room


# batch_token = client.rooms[room_id]._prev_batch - b:all, f:self(?)
# sync_token = client.sync_token - b:all, f:None

class Client:

    def __init__(self, server, username, password):
        self._client = None
        self._server = server
        self._username = username
        self._password = password
        pass

    def sign_in(self, fingerprint=None):
        err = None
        if fingerprint:
            username = ""
            password = ""
        try:
            self._client = MatrixClient(self._server)
            self._client.login(username=self._username, password=self._password)
        except Exception as error:
            err = error
            print(error)
        content = {
            "client": self._client,
            "error": err
        }
        return content

    def sign_up(self, first_name, last_name, millitary_rank, position, fingerprint=None,
                avatar=None):
        err = None

        try:

            self._client = MatrixClient(self._server)
            self._client.register_with_password(username=self._username, password=self._password)

        except Exception as error:
            err = error.__context__
            print(error)

        content = {
            "client": self._client,
            "error": err
        }
        return content

    def log_out(self, client):
        self._client.logout()

    def get_messages(self, alert_of_room, alert_of_room1):
        try:
            time_message = nickname = text_message = ''
            message = []
            client = MatrixClient(self._server)
            matrix = MatrixHttpApi(self._server, self._client.token)
            room_id = matrix.get_room_id(f'#{alert_of_room}:{alert_of_room1}')
            batch_token = self._client.rooms[room_id]._prev_batch
            sync_token = client.sync_token
            s = matrix.get_room_messages(room_id, batch_token, "f", limit=10000)
            for i in range(len(s['chunk'])):
                try:
                    text_message = (s['chunk'][i]['content']['body'])
                    time_message = str(time.localtime(int(str(s['chunk'][i]['origin_server_ts'])[:-3]))[3]) + ':' + str(
                        time.localtime(int(str(s['chunk'][i]['origin_server_ts'])[:-3]))[4]) + ':' + str(
                        time.localtime(int(str(s['chunk'][i]['origin_server_ts'])[:-3]))[5]) + ' ' + str(
                        time.localtime(int(str(s['chunk'][i]['origin_server_ts'])[:-3]))[0]) + '.' + str(
                        time.localtime(int(str(s['chunk'][i]['origin_server_ts'])[:-3]))[1]) + '.' + str(
                        time.localtime(int(str(s['chunk'][i]['origin_server_ts'])[:-3]))[2])
                    nickname = s['chunk'][i]['sender'][1:-11]
                    message.append([time_message, nickname, text_message])
                except:
                    pass
            return message
        except Exception as error:
            return error
