from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.first_page, name='first'),
    url(r'^[a-zA-z_]+/chat/$', views.chat, name='chat'),
    url(r'^check_login/$', views.check_login, name='check_login'),
    url(r'^registration_succes/$', views.registration_success, name='registration_succes'),
    url(r'^admin_login/$', views.admin_login, name='admin_login'),
    url(r'^admin_login_view/$', views.admin_login_view, name='admin_login_view'),
    url(r'^admin_login/super_admin_view/$', views.super_admin_view, name='admin_view'),
    url(r'^admin_login/[a-zA-z0-9_\-]+/admin_view/$', views.admin_view, name='admin_view'),
    url(r'^super_admin_tokens/$', views.super_admin_tokens, name='super_admin_tokens'),
    url(r'^super_admin_change_token/$', views.super_admin_change_token, name='super_admin_change_token'),
    url(r'^load_structure_table/$', views.load_structure_table, name='load_structure_table'),
    url(r'^delete_structure/$', views.delete_structure, name='delete_structure'),
    url(r'^add_structure/$', views.add_structure, name='add_structure'),
    url(r'^show_admins/$', views.show_admins, name='show_admins'),
]
