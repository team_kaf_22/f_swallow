from django.contrib import admin
from . import models

admin.site.register(models.SuperAdmin)
admin.site.register(models.Admin)
admin.site.register(models.Structure)
admin.site.register(models.User)
