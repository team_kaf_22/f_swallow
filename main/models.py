from django.db import models


# Create your models here.

class SuperAdmin(models.Model):
    token = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return f"Super Admin {self.token}"


class Admin(models.Model):
    token = models.CharField(max_length=20, unique=True)
    date_issue = models.DateField(auto_now_add=True)
    is_active = models.BooleanField(default=False)
    structure = models.CharField(max_length=100)

    def __str__(self):
        return f"Admin {self.structure} {self.token}"


class Structure(models.Model):
    name = models.CharField(max_length=100, unique=True)
    abbreviation = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.abbreviation} {self.name}"


class User(models.Model):
    user_name = models.CharField(max_length=20, unique=True)
    date_issue = models.DateField(auto_now_add=True)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    power_structure = models.CharField(max_length=100)
    rank = models.CharField(max_length=20)
    position = models.CharField(max_length=256)
    domain = models.CharField(max_length=15)
    central_domain = models.CharField(max_length=15)
    fingerprint = models.CharField(max_length=300)
    password = models.CharField(max_length=25)

    def __str__(self):
        return f"{self.user_name} - {self.first_name} {self.last_name}"
