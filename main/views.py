from django.db.utils import IntegrityError
from django.http import JsonResponse
from django.shortcuts import render

from . import models
from . import bussiness


def first_page(request):
    return render(request, 'new_start_up.html')


def chat(request):
    return render(request, 'chat.html')


def admin_login(request):
    return render(request, 'admin_login.html')


def admin_view(request):
    return render(request, 'admin.html')


def super_admin_view(request):
    return render(request, 'super_admin.html')


def delete_structure(request):
    if request.method == "GET":
        names = request.GET['value']
        for name in names.split(','):
            models.Structure.objects.filter(name=name).delete()
        data = {
            'info': 'success',
        }
        return JsonResponse(data)


def add_structure(request):
    if request.method == "GET":
        structure = request.GET['new_structure']
        abbreviate = ''.join([x[0] for x in structure if str(x[0]).isupper()])
        models.Structure.objects.create(name=structure, abbreviation=abbreviate)
        data = {
            'info': 'success',
        }
        return JsonResponse(data)


def show_admins(request):
    pw_structure = ''
    if request.method == "GET":
        pw_structure = request.GET['structure_name']
    admin_list = models.Admin.objects.filter(structure=pw_structure)
    data = {
        'info': 'success',
        'admins': admin_list,
    }
    return JsonResponse(data)


def load_structure_table(request):
    if request.method == "GET":
        structures = [stru.name for stru in models.Structure.objects.all()]
        data = {
            'info': 'success',
            'structures': structures,
        }
        return JsonResponse(data)


def super_admin_change_token(request):
    if request.method == "GET":
        new_token = request.GET['token']
        super_admin = models.SuperAdmin.objects.get()
        super_admin.token = new_token
        super_admin.save()
        data = {
            'info': 'success',
        }
        return JsonResponse(data)


def admin_login_view(request):
    if request.method == "GET":
        token = request.GET['token']
        if token == models.SuperAdmin.objects.get().token:
            data = {
                'info': str('super_admin'),
            }
        elif token in [admin.token for admin in models.Admin.objects.all()]:
            data = {
                'structure': [admin.structure for admin in models.Admin.objects.all() if admin.token == token][0],
                'info': str('admin'),
            }
        else:
            data = {
                'info': str('Invalid token!'),
            }
        return JsonResponse(data)


def super_admin_tokens(request):
    if request.method == "GET":
        power_structure = request.GET['structure']
        tokens = request.GET['token'].strip()
        count_unique = 0
        for t in tokens.split('\n'):
            try:
                models.Admin.objects.create(token=t, structure=power_structure)
            except IntegrityError:
                count_unique += 1
        data = {
            "count_unique": str(count_unique),
        }
        return JsonResponse(data)


def check_login(request):
    if request.method == "GET":
        print(request.GET)
        server = "http://" + request.GET['server'] + ":8008"
        username = request.GET['user_id']
        password = request.GET['password']
        client = bussiness.Client(server, username, password)
        connect = client.sign_in()
        data = {
            'user': username,
            'error': connect['error'],
        }
        return JsonResponse(data)


def registration_success(request):
    if request.method == 'POST':
        user_name = request.POST['user_name']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        millitary_rank = request.POST['millitary_rank']
        position = request.POST['position']
        username = request.POST['username']
        username = request.POST['username']
        username = request.POST['username']
        username = request.POST['username']
        username = request.POST['username']
    return None
